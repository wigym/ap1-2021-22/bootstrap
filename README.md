#  Responsive bootstrap demo

##

Start with site JSFiddle at https://jsfiddle.net/

## NavBar

```html
  <!-- navbar -->

  <nav class="navbar navbar-expand-lg fixed-top ">
	  <a class="navbar-brand" href="#">Home</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse " id="navbarSupportedContent">
	    <ul class="navbar-nav mr-4">

	      <li class="nav-item">
	        <a class="nav-link" data-value="about" href="#">About</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " data-value="portfolio" href="#">Portfolio</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " data-value="blog" href="#">Blog</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " data-value="team" href="#">
	        Team</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link " data-value="contact" href="#">Contact</a>
	      </li>
	    </ul>

	  </div>
</nav>
```

## Enable Bootstrap

 * https://code.jquery.com/jquery-3.4.1.slim.js
 * https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.css
 * https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css

## Modify NavBar

```css
.navbar{ background:#F97300;}

.nav-link , .navbar-brand{ color: #f4f4f4; cursor: pointer;}

.nav-link{ margin-right: 1em !important;}

.nav-link:hover{ background: #f4f4f4; color: #f97300; }

.navbar-collapse{ justify-content: flex-end;}

.navbar-toggler{  background:#fff !important;}

.bg-dark{
background-color:#343a40!important
}
.bg-primary{
background-color:#007bff!important
}
```

##  Header

```
<header class="header">

</header>
```

```css
/*header style*/
.header{
 background-image: url('https://gitlab.com/davidhrbac/bootstrap/-/raw/647798422aaa4772e8e2282d978135733e99fd0b/images/headerback.jpg');
 background-attachment: fixed;
 background-size: cover;
 background-position: center;
}
```

Mofidy size by CSS with `height` or by using JavaScript.

```js
$(document).ready(function(){
 $('.header').height($(window).height());

})
```

## Overlay

```html
<header class="header">
  <div class="overlay"></div>
</header>
```

```css
.overlay{
 position: absolute;
 min-height: 100%;
 min-width: 100%;
 left: 0;
 top: 0;
 background: rgba(244, 244, 244, 0.79);
}
```

# Solution

```html
<!-- header -->
<header class="header ">
  <div class="overlay"></div>
   <div class="container">
   	  <div class="description ">
  	<h1>
  		Hello ,Welcome To My official Website
  		<p>
  		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  		<button class="btn btn-outline-secondary btn-lg">See more</button>
  	</h1>
  </div>
   </div>

</header>
```

```css
.description{
    position: absolute;
    top: 30%;
    margin: auto;
    padding: 2em;

}
.description h1{
 color:#F97300 ;
}
.description p{
 color:#666;
 font-size: 20px;
 width: 50%;
 line-height: 1.5;
}
.description button{
 border:1px  solid #F97300;
 background:#F97300;
 color:#fff;
}
```

# About

```html
<div class="about" id="about">
	<div class="container">
	  <h1 class="text-center">About Me</h1>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12">
				<img src="https://gitlab.com/davidhrbac/bootstrap/-/blob/005652eeace32891255e14f50897c5b34ffbf2c8/images/team-3.jpg" class="img-fluid">
				<span class="text-justify">S.Web Developer</span>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12 desc">

				<h3>D.John</h3>
				<p>
				   ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div>
		</div>
	</div>
</div>
```

```css
.about{
	margin: 4em 0;
	padding: 1em;
	position: relative;
}
.about h1{
	color:#F97300;
	margin: 2em;
}
.about img{
	height: 100%;
    width: 100%;
    border-radius: 50%
}
.about span{
	display: block;
	color: #888;
	position: absolute;
	left: 115px;
}
.about .desc{
	padding: 2em;
	border-left:4px solid #10828C;
}
.about .desc h3{
	color: #10828C;
}
.about .desc p{
	line-height:2;
	color:#888;
}
```

# Footer

```html
	<footer class="footer mt-auto py-3" style="background:#F97300;">
	<div class="container">
		<p class="text-center text-white lead">Copyright &copy; <script type="text/javascript">
			document.write(new Date().getFullYear());
		  </script> ABC Company.</p>

	</div>
  </footer>
```
